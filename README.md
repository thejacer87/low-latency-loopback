# Low Latency Loopback

This is a fork of: https://extensions.gnome.org/extension/954/pulseaudio-loopback-device/

1. Changed the loopback time to 1s
2. Different icons

### How to install
1. Clone this repo or download a tar/zip containing this repo
2. Copy folder 'Low_Latency_Loopback@jacebennest87.gmail.com' to your local extensions dir (~/.local/share/gnome-shell/extensions/)
3. Enjoy

### Credits
- [jocagovi](https://extensions.gnome.org/accounts/profile/jocagovi) for the initial extension
- [ragingraging](https://extensions.gnome.org/accounts/profile/ragingraging) for the additional updates and fixes
